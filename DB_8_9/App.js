const express = require("express");

const app = express();

const urlencodedParser = express.urlencoded({extended: false});

const MongoClient = require("mongodb").MongoClient;

app.get("/", function (request, response) {
    response.sendFile(__dirname + "/index2.html");
});

app.post("/articles_list", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");

        collection.aggregate([{$project: {_id: 1, title: 1, autor: 1, date_of_public: 1}}])
            .toArray(function(err, results){
                let a = "";
                for(let i = 0; i < results.length; i++){
                    a = a + "<br>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public
                          + "<form action=\"/state" + i +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Подробная информация\" />" + "</p>" + "</form>"
                          + "<form action=\"/delete" + i +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\" />" + "</p>" + "</form>"
                          + "</br>";
                    app.post("/state" + i, urlencodedParser, function (request, response) {
                        async function connectToDatabase() {
                            const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                            const db = client.db("FirstBase");
                            const collection = db.collection("articles");

                            if(err) return console.log(err);

                            collection.aggregate([{$project: {_id: 1, title: 1, autor: 1, date_of_public: 1, text:1, tags:1, comments:1}}])
                                .toArray(function(err, results){
                                    let c = "";
                                    for (let j = 0; j < results[i].comments.length; j++){
                                        c = c + "[Имя пользователя: " + results[i].comments[j].name + "<br>" + "Текст комментария: " + results[i].comments[j].text_comment+ "<br>" + "Оценка: " + results[i].comments[j].raiting + "], " + "<br>";
                                    }
                                    let b = "<p>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public + "<br>" + "Текст статьи: " + results[i].text + "<br>" + "<br>" + "Теги: " + results[i].tags + "<br>" + "<br>" + "Комментраии пользователей: " + "<br>" + c + "</p>"
                                    response.send(b);
                                    client.close();
                                });
                        };
                        connectToDatabase();
                    });

                    app.post("/delete" + i, urlencodedParser, function (request, response) {
                        async function connectToDatabase() {
                            const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                            const db = client.db("FirstBase");
                            const collection = db.collection("articles");

                            if (err) return console.log(err);

                            collection.deleteOne({_id: results[i]._id});

                            collection.aggregate([{$project: {_id: 1, title: 1, autor: 1, date_of_public: 1}}])
                                .toArray(function (err, results) {
                                    let c = "";
                                    for(let i = 0; i < results.length; i++) {
                                        c = c + "<br>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public
                                            + "<form action=\"/state" + i + "\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Подробная информация\" />" + "</p>" + "</form>"
                                            + "<form action=\"/delete" + i + "\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\" />" + "</p>" + "</form>"
                                            + "</br>";
                                    }
                                    response.send(c);
                                    client.close();
                                });
                        };
                        connectToDatabase();
                    });
                }
                response.send(a);
                client.close();
            });
        };
        connectToDatabase();
});

app.post("/articles_title", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");
        let b = request.body.text;

        let reg = ".*" + b + ".*";

        collection.find({
            title: {
                $regex: reg,
                $options: 'i'
            }
        })
            .toArray(function(err, results){
                console.log(results)
                let a = "";
                for(let i = 0; i < results.length; i++){
                    a = a + "<p>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public + "</p>"
                }
                response.send(a);
                client.close();
            });
        };
        connectToDatabase();
});

app.post("/articles_autors", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");

        let b = request.body.aut;
        console.log(b);

        collection.find({autor: b}).toArray(function(err, results){
            console.log(results)
            let a = "";
            for(let i = 0; i < results.length; i++){
                a = a + "<p>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public + "</p>"
            }
            response.send(a);
            client.close();
        });
    };
    connectToDatabase();
});

app.post("/articles_rate", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");

        collection.aggregate([{$project: {_id: 1, title: 1, autor: 1, date_of_public: 1, comments: 1}}])
            .toArray(function (err, results) {
            let a = [];
            let d = [];
            let t = [];
            for(let i = 0; i < results.length; i++){
                let sum = 0;
                for(let j = 0; j < results[i].comments.length; j++){
                    sum = sum + results[i].comments[j].raiting;
                }
                d.push(sum/results[i].comments.length);
                a.push("<p>" + "Номер: " + results[i]._id  + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public + "<br>" + "Рейтинг: " + sum/results[i].comments.length + "<br>" + "Количество комментариев: " + results[i].comments.length + "</p>");
                t.push(results[i].comments.length);
            }
            for (let i = 0; i < a.length; i++){
                for (let j = 0; j < a.length; j++){
                    if (d[i] > d[j]){
                        let c = a[i];
                        a[i] = a[j];
                        a[j] = c;
                        c = d[i];
                        d[i] = d[j];
                        d[j] = c;
                        c = t[i];
                        t[i] = t[j];
                        t[j] = c;
                    }
                }
            }
                for (let i = 0; i < a.length; i++){
                    for (let j = 0; j < a.length; j++){
                        if(d[i] === d[j]){
                            if (t[i] > t[j]) {
                                let c = a[i];
                                a[i] = a[j];
                                a[j] = c;
                                c = d[i];
                                d[i] = d[j];
                                d[j] = c;
                                c = t[i];
                                t[i] = t[j];
                                t[j] = c;
                            }
                        }
                    }
                }
            let k = "";
            for(let i = 0; i < results.length; i++){
                k = k + a[i];
            }
            response.send(k);
            client.close();
        });
    };
    connectToDatabase();
});

app.post("/articles_dates", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");

        let b = request.body.dat1;
        let c = request.body.dat2;

        collection.find({date_of_public: {$gte: new Date("\"" + b + "\""), $lte: new Date("\"" + c + "\"")}}).toArray(function(err, results){
            console.log(results)
            let a = "";
            for(let i = 0; i < results.length; i++){
                a = a + "<p>" + "Номер: " + results[i]._id + "<br>" + "Название: " + results[i].title + "<br>" + "Автор: " + results[i].autor + "<br>" + "Дата публикации: " + results[i].date_of_public + "</p>"
            }
            response.send(a);
            client.close();
        });
    };
    connectToDatabase();
});

app.post("/p_insert", urlencodedParser, function (request, response) {
        response.sendFile(__dirname + "/index.html");
});

app.post("/insert", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("FirstBase");
        const collection = db.collection("articles");

        let title_ = request.body.title;
        let autor_ = request.body.autor;
        let date_ = request.body.date;
        let text_ = request.body.text_;
        let tags_ = request.body.tags;

        let user = {
            title: title_,
            autor: autor_,
            date_of_public: new Date("\"" + date_ + "\""),
            text: text_,
            tags: tags_.split(" "),
            comments: [],
        };

        collection.insertOne(user, function( result){
            client.close();
        });
    response.sendFile(__dirname + "/index2.html");
    };
    connectToDatabase();
});

app.listen(3000);