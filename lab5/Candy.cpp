#include <iostream>
#include "Candy.h"
using namespace std;
void* Candy_init(Candy B)
{
  Candy* A = new Candy;
  A->size = B.size;
  A->price = B.price;
  A->weight = B.weight;
  A->color = B.color;
  void* S = (void*) A;
  return S;
}
void Candy_del(void* S)
{
  delete (Candy*) S;
}
void Candy_out(DLList List)
{
  Candy A;
  List.MoveFirst();
  system("cls");
  if(!List.Count)
  {
    cout << "List is empty" << endl;
    return;
  }
  for(int i = 0; i < List.Count; i++)
  {
    A = *((Candy*) List.C->data);
    cout << "---------------------------------------" << endl;
    cout << "size = " << A.size << endl;
    cout << "price = " << A.price << endl;
    cout << "weight = " << A.weight << endl;
    cout << "color = " << A.color << endl;
    cout << "---------------------------------------" << endl;
    List.MoveNext();
  }
}
void Candy_cin(Candy& A)
{
  cout << "Enter your data:" << endl;
  cout << "size = "; cin >> A.size;
  cout << "price = "; cin >> A.price;
  cout << "weight = "; cin >> A.weight;
  cout << "color = "; cin >> A.color;
}
