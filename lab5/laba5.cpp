﻿#include <iostream>
#include <fstream>
#include <sstream>
#include "DLList.h"
#include "Candy.h"
using namespace std;
int main()
{
    DLList List;
    void* S;
    Candy A;
    int m = 1;
    ifstream file("lab5.txt");
    string line;
    while (getline(file, line))
    {
        istringstream line_F(line);
        line_F >> A.size >> A.price >> A.weight >> A.color;
        S = Candy_init(A);
        List.AddNext(S);
    }
    file.close();
    while (m)
    {
        system("cls");
        cout << "Add element:                         " << endl;
        cout << "11 in the beginning                   " << endl;
        cout << "12 in the end                         " << endl;
        cout << "13 in the k-position                  " << endl << endl;
        cout << "Delete element:                      " << endl;
        cout << "21 the first                          " << endl;
        cout << "22 the last                           " << endl;
        cout << "23 the k position                     " << endl << endl;
        cout << "3 Sort by \"size\"                   " << endl << endl;
        cout << "4 Clear the list                     " << endl << endl;
        cout << "5 Upload data from the list to a file" << endl << endl;
        cout << "6 Show the list                     " << endl << endl;
        cout << "0 Exit                               " << endl;
        cin >> m;
        switch (m)
        {
        case 11:
        {
            system("cls");
            Candy_cin(A);
            S = Candy_init(A);
            List.AddFirst(S);
            Candy_out(List);
            system("pause");
            break;
        }
        case 12:
        {
            system("cls");
            Candy_cin(A);
            S = Candy_init(A);
            List.AddLast(S);
            Candy_out(List);
            system("pause");
            break;
        }
        case 13:
        {
            system("cls");
            int k;
            cout << "Enter the position number from " << 1 << " to " << List.Count + 1 << endl;
            cout << "k = "; cin >> k;
            k--;
            if (k >= 0 & k <= List.Count)
            {
                Candy_cin(A);
                S = Candy_init(A);
                if (k == 0)
                {
                    List.AddFirst(S);
                }
                else
                {
                    if (k == List.Count)
                    {
                        List.AddLast(S);
                    }
                    else
                    {
                        List.Move_k(k - 1);
                        List.AddNext(S);
                    }
                }
                Candy_out(List);
            }
            else cout << "error" << endl;
            system("pause");
            break;
        }
        case 21:
        {
            system("cls");
            if (List.DelFirst(S))
            {
                Candy_del(S);
                Candy_out(List);
            }
            else
            {
                cout << "error" << endl;
            }
            system("pause");
            break;
        }
        case 22:
        {
            system("cls");
            if (List.DelLast(S))
            {
                Candy_del(S);
                Candy_out(List);
            }
            else
            {
                cout << "error" << endl;
            }
            system("pause");
            break;
        }
        case 23:
        {
            system("cls");
            int k;
            if (!List.Count)
            {
                cout << "error" << endl;
                system("pause");
                break;
            }
            cout << "Enter the position number from " << 1 << " to " << List.Count << endl;
            cout << "k = "; cin >> k;
            k--;
            if (k >= 0 & k <= List.Count)
            {
                if (k == 0)
                {
                    List.DelFirst(S);
                }
                else
                {
                    if (k == (List.Count - 1))
                    {
                        List.DelLast(S);
                    }
                    else
                    {
                        List.Move_k(k);
                        List.Del(S);
                    }
                }
                Candy_out(List);
            }
            else cout << "error" << endl;
            system("pause");
            break;
        }
        case 3:
        {
            system("cls");
            void* tmp;
            for (int i = 0; i < List.Count; i++)
            {
                List.MoveLast();
                for (int j = (List.Count - 1); j >= (i + 1); j--)
                {
                    if (((Candy*)List.C->data)->size < ((Candy*)List.C->prev->data)->size)
                    {
                        tmp = List.C->data;
                        List.C->data = List.C->prev->data;
                        List.C->prev->data = tmp;
                    }
                    List.MovePrev();
                }
            }
            Candy_out(List);
            system("pause");
            break;
        }
        case 4:
        {
            List.MoveFirst();
            while (List.Del(S))
            {
                Candy_del(S);
            }
            Candy_out(List);
            system("pause");
            break;
        }
        case 5:
        {
            ofstream file("lab5.txt");
            List.MoveFirst();
            for (int i = 0; i < List.Count; i++)
            {
                A = *((Candy*)List.C->data);
                file << A.size << " " << A.price << " " << A.weight << " " << A.color << endl;
                List.MoveNext();
            }
            file.close();
            cout << "Uploaded!!!" << endl;
            system("pause");
            break;
        }
        case 6:
        {
            system("cls");;
            Candy_out(List);
            system("pause");
            break;
        }
        }
    }
    List.MoveFirst();
    while (List.Del(S))
    {
        Candy_del(S);
    }
    system("pause");
    return 0;
}
