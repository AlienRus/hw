#include <iostream>
#include "DLList.h"
using namespace std;
#ifndef Candy_h
#define Candy_h
struct Candy
{
  double size;
  double price;
  double weight;
  string color;
};
void* Candy_init(Candy);
void Candy_del(void*);
void Candy_out(DLList);
void Candy_cin(Candy&);
#endif
