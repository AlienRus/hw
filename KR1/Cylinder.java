package Figure3D;
import Figure.Figure;

public class Cylinder<typeFigure> implements Figure3D
{
    private typeFigure f;
    private double h;

    public Cylinder(typeFigure f, double h) throws Exception
    {
        if (h > 0) 
        {
			this.h = h;
			this.f = f;
		}
        else throw new Exception("Ошибка создания цилиндра");
    }

    public double calcVolume()
    {
        return h * ((Figure) f).calcArea();
    }
};