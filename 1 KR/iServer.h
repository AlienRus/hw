#pragma once
#include "IUnknown.h"

class iServer : public IUnknown
{
public:
    virtual void Func() = 0;
};

class iServer2 : public IUnknown
{
public:
    virtual void Func2() = 0;
};