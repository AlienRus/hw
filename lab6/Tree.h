#include <iostream>
#include "Pie.h"
using namespace std;
#ifndef Tree_h
#define Tree_h
struct Tree
{
  Pie* data;
  Tree* left;
  Tree* right;
};
bool sort_str(string, string);
void Add_price(Tree*&, Pie*);
void Add_color(Tree*&, Pie*);
void Print(Tree*);
void Delete(Tree*&);
void Print_price(Tree*, float);
void Print_color(Tree*, char);
#endif
