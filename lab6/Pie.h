#include <iostream>
using namespace std;
#ifndef Pie_h
#define Pie_h
struct Pie
{
  string filling;
  float weight;
  string restaurant;
  float price;
};
Pie* Init(Pie);
void Del(Pie*);
void A_Print(Pie*);
#endif
