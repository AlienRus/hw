#include <iostream>
#include "Pie.h"
using namespace std;
Pie* Init(Pie A)
{
  Pie* B = new Pie;
  *B = A;
  return B;
}
void Del(Pie* A)
{
  delete A;
}
void A_Print(Pie* A)
{
  cout << endl << endl;
  cout << "Filling: " << A->filling << endl;
  cout << "Weight: " << A->weight << endl;
  cout << "Restaurant: " << A->restaurant << endl;
  cout << "Price: " << A->price << endl;
  cout << endl << endl;
}
