﻿#include <iostream>
#include "3D_Figure.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int select;
	Figure* F = NULL;
	cout << "Выбрать фигуру: (1 - Круг, 2 - Треугольник, 3 - Кольцо): ";
	cin >> select;
	switch (select)
	{
	case 1:
	{
		double r;
		cout << "Радиус:\n";
		cout << "r = "; cin >> r;
		F = new Circle(r);
		break;
	}
	case 2:
	{
		double a, b, c;
		cout << "Стороны треугольника:\n";
		cout << "a = "; cin >> a;
		cout << "b = "; cin >> b;
		cout << "c = "; cin >> c;
		F = new Triangle(a, b, c);
		break;
	}
	case 3:
	{
		double r1, r2;
		cout << "Радиусы кольца:\n";
		cout << "r1 = "; cin >> r1;
		cout << "r2 = "; cin >> r2;
		F = new Ring(r1, r2);
		break;
	}
	default:
		delete F;
		return 0;
		break;
	}
	cout << "Высота цилиндра:\n";
	int h;
	cout << "h = "; cin >> h;
	Cylinder C(F, h);
	cout << "Полученный объем фигуры:\n";
	cout << C.Volume();
	delete F;
}