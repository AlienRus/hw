#include "Wrapper.h"
#include <windows.h>
HINSTANCE h;

Wrapper::Wrapper()
{
    FunctionType Co_create_instance;
    h = LoadLibrary("C++/Manager.dll");
    std::cout << "111";
    if (!h)
    {
        std::cout << "no dll!!!!!" << std::endl;
    }

    Co_create_instance = (FunctionType)GetProcAddress(h, "Co_CreateInstance");
    if (!Co_create_instance)
    {
        std::cout << "no dll func!!!!!" << std::endl;
    }
    Co_create_instance(clsidServ, iid_IClassFactory, (void **)&enterMatr);
    enterMatr->QueryInterface(iid_IEnter, (void **)&enterMatr);
    enterMatr->QueryInterface(iid_ITandP, (void **)&TandP);

    system("pause");
}

void Wrapper::enter()
{
    int n, m;
    std::cin >> n;
    std::cin >> m;
    enterMatr->EnterMatrix(n, m);
}
void Wrapper::tranPrint()
{
    TandP->PrintMatrix();
    TandP->TransposeMatrix();
    TandP->PrintMatrix();
}
void Wrapper::multyply()
{
    TM->multyply();
}
Wrapper::Wrapper(const Wrapper &other)
{
    enterMatr = other.enterMatr;
    enterMatr->AddRef();
    TandP = other.TandP;
    TandP->AddRef();
}
Wrapper::~Wrapper()
{
    enterMatr->Release();
    TandP->Release();
    enterMatr->Release();
    FreeLibrary(h);
    system("pause");
}