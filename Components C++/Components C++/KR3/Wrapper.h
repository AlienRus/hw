#include "Server.h"
#include <iostream>

typedef H_RESULT (*FunctionType)(CLS_ID, I_ID, void **);

class Wrapper
{
private:
    IEnterIntMatrix *enterMatr = NULL;
    ITransposeAndPrintAnyMatrix *TandP = NULL;

public:
    Wrapper();
    Wrapper(const Wrapper &other);
    ~Wrapper();
    void enter();
    void tranPrint();
};