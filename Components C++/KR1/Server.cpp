#include <iostream>
#include <iomanip>
#include <windows.h>
#include "Server.h"
using namespace std;

void Component::NewMemoryForIntMatrix()
{
    std::cin >> this->n;
    std::cin >> this->m;
    matrix = new int *[n];
    for (int i = 0; i < n; i++)
    {
        matrix[i] = new int[m];
    }
}

void Component::DelMemoryForIntMatrix()
{
    for (int i = 0; i < n; i++)
    {
        delete[] matrix[i];
    }
    delete[] matrix;
}
void Component::EnterMatrix()
{
    NewMemoryForIntMatrix();
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            std::cout << "Enter element with index:" << i << " " << j << std::endl;
            std::cin >> matrix[i][j];
        }
    }
}

void Component::TransposeMatrix()
{

    int buff;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            buff = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = buff;
        }
    }
}

void Component::PrintMatrix()
{
    std::cout << std::endl;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            std::cout << std::setw(15) << matrix[i][j];
        }
        std::cout << std::endl;
    }
}

ULONG_ Component::AddRef()
{
    this->count++;
    std::cout << "\nComponent object " << this << " added; count:" << this->count << std::endl;
    return count;
}
ULONG_ Component::Release()
{
    this->count--;
    std::cout << "\nComponent object " << this << " released; count:" << this->count << std::endl;
    if (this->count == 0)
    {
        delete this;
    }
    return count;
}

Component::~Component()
{
    this->DelMemoryForIntMatrix();
}

Factory::~Factory()
{
}

ULONG_ Factory::AddRef()
{
    this->count++;
    std::cout << "\nFactory object " << this << " added; count:" << this->count << std::endl;
    return count;
}
ULONG_ Factory::Release()
{
    this->count--;
    std::cout << "\nFactory object " << this << " released; count:" << this->count << std::endl;
    if (this->count == 0)
    {
        delete this;
    }
    return count;
}

H_RESULT Component::QueryInterface(I_ID iid, void **ppv)
{
    if (iid == iid_IUnknown_)
    {
        *ppv = (IUnknown_ *)(IEnterIntMatrix *)this;
    }
    else if (iid == iid_IEnter)
    {
        *ppv = (IEnterIntMatrix *)this;
    }
    else if (iid == iid_ITandP)
    {
        *ppv = (ITransposeAndPrintAnyMatrix *)this;
    }
    else
    {
        ppv = NULL;
        return E_NOINTERFACE__;
    }
    AddRef();
    return S_OK__;
}

extern "C" H_RESULT __declspec(dllexport) Factory::CreateInstance(I_ID iid, void **ppv)
{
    Component *comp = new Component();

    comp->QueryInterface(iid, ppv);
    return 0;
}

H_RESULT Factory::QueryInterface(I_ID iid, void **ppv)
{
    if (iid == iid_IUnknown_)
    {
        *ppv = (IClassFactory_ *)this;
    }
    else if (iid == iid_IClassFactory)
    {
        *ppv = (IClassFactory_ *)this;
    }
    else
    {
        *ppv = NULL;
        return E_NOINTERFACE__;
    }
    AddRef();
    return S_OK__;
}
extern "C" H_RESULT __declspec(dllexport) GetClassObject(CLS_ID servid, I_ID IClassFactory_id, void **ppv)
{
    if (servid == clsidServ)
    {
        Factory *fact = new Factory();
        fact->QueryInterface(IClassFactory_id, ppv);
    }
    return 0;
}

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        break;

    case DLL_PROCESS_DETACH:
        break;

    case DLL_THREAD_ATTACH:
        break;

    case DLL_THREAD_DETACH:
        break;
    }
    return TRUE; 
}