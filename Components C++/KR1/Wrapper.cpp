#include "Wrapper.h"
#include <windows.h>

Server::Server()
{
    FunctionType f;
    HINSTANCE h;
    h = LoadLibrary("C++/Server.dll");
    if (!h)
    {
        std::cout << "no dll" << std::endl;
    }

    f = (FunctionType)GetProcAddress(h, "GetClassObject");
    if (!f)
    {
        std::cout << "no dll func" << std::endl;
    }
    
    f(clsidServ, iid_IClassFactory, (void **)&fact);

    fact->CreateInstance(iid_IEnter, (void **)&enterMatr);

    enterMatr->QueryInterface(iid_ITandP, (void **)&TandP);

    system("pause");
}

void Server::enter()
{

    enterMatr->EnterMatrix();
}
void Server::tranPrint()
{
    TandP->PrintMatrix();
    TandP->TransposeMatrix();
    TandP->PrintMatrix();
}
Server::Server(const Server &other)
{
    fact = other.fact;
    fact->AddRef();
    enterMatr = other.enterMatr;
    enterMatr->AddRef();
    TandP = other.TandP;
    TandP->AddRef();
}
Server::~Server()
{
    enterMatr->Release();
    TandP->Release();
    fact->Release();
}