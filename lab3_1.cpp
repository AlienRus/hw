﻿#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
struct watch
{
    string Model;
    int Diagonal;
    string Type;
    string Color;
};
struct MyStack
{
    struct Node
    {
        watch data;
        Node* prev;
    };
    Node* Top = NULL;
    int Count = 0;
    bool Push(watch);
    bool Pop(watch&);
    void Info();
};
bool MyStack::Push(watch dt)
{
    if (!Top)
    {
        Top = new Node;
        Top->prev = NULL;
        Count = 1;
    }
    else
    {
        Node* temp;
        temp = new Node;
        temp->prev = Top;
        Top = temp;
        Count++;
    }
    Top->data = dt;
    return true;
}
bool MyStack::Pop(watch& dt)
{
    if (!Top) return false;
    Node* temp = Top->prev;
    dt = Top->data;
    delete Top;
    Top = temp;
    Count--;
    return true;
}
void MyStack::Info()
{
    if (!Top) cout << "Stack is empty" << endl;
    else
    {
        cout << endl << "Stack info: " << endl;
        cout << "\tStack size = " << Count << endl;
        cout << "\tTop Model = " << Top->data.Model << endl;
        cout << "\tTop Diagonal  = " << Top->data.Diagonal  << endl;
        cout << "\tTop Type  = " << Top->data.Type  << endl;
        cout << "\tTop Color = " << Top->data.Color << endl << endl;
    }
}
void print(MyStack& S, MyStack& V)
{
    watch dt;
    while (S.Count)
    {
        S.Pop(dt);
        cout << "\tModel = " << dt.Model << endl;
        cout << "\tDiagonal  = " << dt.Diagonal  << endl;
        cout << "\tType  = " << dt.Type  << endl;
        cout << "\tColor = " << dt.Color << endl << endl;
        V.Push(dt);
    }
    while (V.Count)
    {
        V.Pop(dt);
        S.Push(dt);
    }
}
int main()
{
    MyStack S;
    MyStack V;
    watch dt;
    ifstream file("watch.txt");
    string line;
    while (getline(file, line))
    {
        istringstream line_F(line);
        line_F >> dt.Model >> dt.Diagonal  >> dt.Type  >> dt.Color;
        S.Push(dt);
    }
    file.close();
    int m = 1;
    while (m)
    {
        S.Info();
        cout << "1. Add product to basket" << endl;
        cout << "2. Pull an item from the basket" << endl;
        cout << "3. Clear the basket" << endl;
        cout << "4. Show basket contents" << endl;
        cout << "0. Exit" << endl;
        cin >> m;
        switch (m)
        {
        case 1:
        {
            system("cls");
            cout << "Enter product specifications:" << endl;
            cout << "Model = "; cin >> dt.Model;
            cout << "Diagonal  = "; cin >> dt.Diagonal ;
            cout << "Type  = "; cin >> dt.Type ;
            cout << "Color = "; cin >> dt.Color;
            S.Push(dt);
            system("cls");
            break;
        }
        case 2:
        {
            system("cls");
            watch dt_x;
            bool metka = false;
            cout << "Enter product specifications:" << endl;
            cout << "Model = "; cin >> dt_x.Model;
            cout << "Diagonal  = "; cin >> dt_x.Diagonal ;
            cout << "Type  = "; cin >> dt_x.Type ;
            cout << "Color = "; cin >> dt_x.Color;
            while (S.Count)
            {
                S.Pop(dt);
                if (dt.Model != dt_x.Model || dt.Diagonal  != dt_x.Diagonal  || dt.Type  != dt_x.Type  || dt.Color != dt_x.Color)
                {
                    V.Push(dt);
                }
                else
                {
                    metka = true;
                    break;
                }
            }
            if (!metka) cout << "Product not found" << endl;
            while (V.Count)
            {
                V.Pop(dt);
                S.Push(dt);
            }
            system("pause");
            system("cls");
            break;
        }
        case 3:
        {
            system("cls");
            while (S.Count)
            {
                S.Pop(dt);
            }
            break;
        }
        case 4:
        {
            system("cls");
            print(S, V);
            system("pause");
            system("cls");
            break;
        }
        }
    }
}