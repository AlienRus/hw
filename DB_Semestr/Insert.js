db.Sports_facilities.drop()
db.Types_of_sports.drop()
db.Coaches.drop()
db.Athletes.drop()
db.Competitions.drop()
db.Awards.drop()

//----------------------Types_of_sports-------------------------------------------
let Types_of_sports_1 = db.Types_of_sports.insertOne({title: "Волейбол"})
let Types_of_sports_2 = db.Types_of_sports.insertOne({title: "Баскетбол"})
let Types_of_sports_3 = db.Types_of_sports.insertOne({title: "Футбол"})
let Types_of_sports_4 = db.Types_of_sports.insertOne({title: "Легкая атлетика"})
let Types_of_sports_5 = db.Types_of_sports.insertOne({title: "Карате"})

//-----------------------Sports_facilities---------------------------------------
let Sports_facilities_1 = db.Sports_facilities.insertOne({
    title: "Лазурный",
    address: "Октябрьский 2",
    category: "Спортивный комплекс",
    attributes: [{
        title:"Объем бассейна",
        meaning: 1000,
        measure_unit: "литры",
    },
        {
            title:"Вместимость",
            meaning:1000,
            measure_unit: "люди",
        }]
    })
let Sports_facilities_2 = db.Sports_facilities.insertOne({
    title: "Детство",
    address: "Советский 42",
    category: "Корт",
    attributes: [{
        title:"Вместимость",
        meaning:300,
        measure_unit: "люди",
    },
        {
            title:"Категория покрытия",
            meaning:4,
        }]
    })
let Sports_facilities_3 = db.Sports_facilities.insertOne({
    title: "Улыбка",
    address: "Проспект шахтеров 10",
    category: "Комплекс",
    attributes: [{
        title:"Площадь",
        meaning:20000,
        measure_unit: "кв. м",
    },
        {
            title:"Количество видов спорта",
            meaning:20,
        }]
    })
let Sports_facilities_4 = db.Sports_facilities.insertOne({
    title: "Белый кит",
    address: "Весенняя 20",
    category: "Плавательный корт",
    attributes: [{
        title:"Количество доржек",
        meaning:10,
    },
        {
            title:"Вместимость",
            meaning:300,
            measure_unit: "люди",
        }]
    })
let Sports_facilities_5 = db.Sports_facilities.insertOne({
    title: "Будущее",
    address: "Октябрьский 40",
    category: "Стадион",
    attributes: [{
        title:"Вместимость",
        meaning:3000,
        measure_unit: "люди",
    },
        {
            title:"Площадь",
            meaning:5000,
            measure_unit: "кв. м",
        }]})


//----------------------Coaches-------------------------------

let Coaches_1 = db.Coaches.insertOne({
    first_name: "Анна",
    last_name: "Ярощук",
    patronymic: "Петровна",
    work_experience: 10,
    sport_type: Types_of_sports_2.insertedId //ссылается на виды спорта
    })
let Coaches_2 = db.Coaches.insertOne({
    first_name: "Николай",
    last_name: "Костервальд",
    patronymic: "Евгеньевич",
    work_experience: 23,
    sport_type: Types_of_sports_1.insertedId
})
let Coaches_3 = db.Coaches.insertOne({
    first_name: "Ярослав",
    last_name: "Иронив",
    patronymic: "Антонович",
    work_experience: 5,
    sport_type: Types_of_sports_5.insertedId
})
let Coaches_4 = db.Coaches.insertOne({
    first_name: "Инна",
    last_name: "Соболева",
    patronymic: "Николаевна",
    work_experience: 6,
    sport_type: Types_of_sports_2.insertedId
})
let Coaches_5 = db.Coaches.insertOne({
    first_name: "Антон",
    last_name: "Андор",
    patronymic: "Андреевич",
    work_experience: 2,
    sport_type: Types_of_sports_1.insertedId
})
let Coaches_6 = db.Coaches.insertOne({
    first_name: "Полина",
    last_name: "Синих",
    patronymic: "Яковлевна",
    work_experience: 21,
    sport_type: Types_of_sports_3.insertedId
})
let Coaches_7 = db.Coaches.insertOne({
    first_name: "Валентин",
    last_name: "Ярких",
    patronymic: "Максимович",
    work_experience: 13,
    sport_type: Types_of_sports_4.insertedId
})

//----------------------Athletes--------------------------------------------
let Athletes_1 = db.Athletes.insertOne({
    first_name: "Иван",
    last_name: "Визиров",
    patronymic: "Олегович",
    date_of_birth: new Date("02.03.2001"),
    types_of_sports: [{
        sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_2.insertedId }), //дублируется из таблицы виды спорта
        sports_category: 5, //разряд
        coaching_staff: [
            db.Coaches.findOne({ _id: Coaches_1.insertedId }), //дублируется из таблицы тренеров
            db.Coaches.findOne({ _id: Coaches_4.insertedId }),
        ],
    },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_1.insertedId }),
            sports_category: 4,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_2.insertedId }),
            ],
        }],
    sports_club: "Cool Cloub",
})
let Athletes_2 = db.Athletes.insertOne({
    first_name: "Иван",
    last_name: "Ровков",
    patronymic: "Ктототамович",
    date_of_birth: new Date("03.03.2002"),
    types_of_sports: [{
        sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_3.insertedId }),
        sports_category: 3,
        coaching_staff: [
            db.Coaches.findOne({ _id: Coaches_6.insertedId }),
        ],
    }],
    sports_club: "Спорт - жизнь",
})
let Athletes_3 = db.Athletes.insertOne({
    first_name: "Александр",
    last_name: "Пасютин",
    patronymic: "Сергеевич",
    date_of_birth: new Date("01.05.2001"),
    types_of_sports: [{
        sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_5.insertedId }),
        sports_category: 2,
        coaching_staff: [
            db.Coaches.findOne({ _id: Coaches_3.insertedId }),
        ],
    },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_2.insertedId }),
            sports_category: 5,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_5.insertedId }),
            ],
        }],
    sports_club: "Фестиваль",
})
let Athletes_4 = db.Athletes.insertOne({
    first_name: "Соболев",
    last_name: "Дмитрий",
    patronymic: "Кринжовович",
    date_of_birth: new Date("01.01.2002"),
    types_of_sports: [{
        sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_1.insertedId }),
        sports_category: 1,
        coaching_staff: [
            db.Coaches.findOne({ _id: Coaches_2.insertedId }),
            db.Coaches.findOne({ _id: Coaches_1.insertedId }),
        ],
    },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_2.insertedId }),
            sports_category: 1,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_1.insertedId }),
                db.Coaches.findOne({ _id: Coaches_4.insertedId }),
            ],
        },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_5.insertedId }),
            sports_category: 1,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_3.insertedId }),
            ],
        }],
    sports_club: "Фестиваль",
})
let Athletes_5 = db.Athletes.insertOne({
    first_name: "Владислав",
    last_name: "Сергеев",
    patronymic: "Викторович",
    date_of_birth: new Date("02.02.2002"),
    types_of_sports: [{
        sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_4.insertedId }),
        sports_category: 5,
        coaching_staff: [
            db.Coaches.findOne({ _id: Coaches_3.insertedId }),
        ],
    },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_3.insertedId }),
            sports_category: 5,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_6.insertedId }),
            ],
        },
        {
            sport_type: db.Types_of_sports.findOne({ _id: Types_of_sports_1.insertedId }),
            sports_category: 5,
            coaching_staff: [
                db.Coaches.findOne({ _id: Coaches_2.insertedId }),
            ],
        }],
    sports_club: "Лазурь",
})

//----------------------Competitions------------------------------------------
let Competitions_1 = db.Competitions.insertOne({
    title: "Чемпионат по плаванию 2021 года",
    date_of_the_event: new Date("12.12.2021"),
    sports_facility: Sports_facilities_3.insertedId, //ссылается на таблицу спортивных сооружений
    sport_type: Types_of_sports_1.insertedId, //ссылается на таблицу виды спорта
    sports_organization: "Победа",
    athletes: [
        db.Athletes.findOne({ _id: Athletes_1.insertedId }), //дублируется таблица атлетов
        db.Athletes.findOne({ _id: Athletes_3.insertedId }),
        db.Athletes.findOne({ _id: Athletes_5.insertedId }),
    ]
})
let Competitions_2 = db.Competitions.insertOne({
    title: "Чемпионат по метанию тарелок",
    date_of_the_event: new Date("01.01.2022"),
    sports_facility: Sports_facilities_2.insertedId,
    sport_type: Types_of_sports_4.insertedId,
    sports_organization: "Дичь",
    athletes: [
        db.Athletes.findOne({ _id: Athletes_1.insertedId }),
        db.Athletes.findOne({ _id: Athletes_4.insertedId }),
        db.Athletes.findOne({ _id: Athletes_5.insertedId }),
    ]
})
let Competitions_3 = db.Competitions.insertOne({
    title: "ЗаБЕГ",
    date_of_the_event: new Date("02.03.2020"),
    sports_facility: Sports_facilities_1.insertedId,
    sport_type: Types_of_sports_3.insertedId,
    sports_organization: "Скорость",
    athletes: [
        db.Athletes.findOne({ _id: Athletes_2.insertedId }),
        db.Athletes.findOne({ _id: Athletes_4.insertedId }),
        db.Athletes.findOne({ _id: Athletes_1.insertedId }),
    ]
})
let Competitions_4 = db.Competitions.insertOne({
    title: "Чемпионат по карате",
    date_of_the_event: new Date("06.03.2018"),
    sports_facility: Sports_facilities_5.insertedId,
    sport_type: Types_of_sports_5.insertedId,
    sports_organization: "Бой",
    athletes: [
        db.Athletes.findOne({ _id: Athletes_4.insertedId }),
        db.Athletes.findOne({ _id: Athletes_1.insertedId }),
    ]
})
let Competitions_5 = db.Competitions.insertOne({
    title: "Гибкость 2022",
    date_of_the_event: new Date("01.11.2022"),
    sports_facility: Sports_facilities_4.insertedId,
    sport_type: Types_of_sports_4.insertedId,
    sports_organization: "Гибкость",
    athletes: [
        db.Athletes.findOne({ _id: Athletes_3.insertedId }),
        db.Athletes.findOne({ _id: Athletes_2.insertedId }),
    ]
})

//----------------------Awards-------------------------------------------------------------------------------------
let Awards_1 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_1.insertedId,
    athlet: Athletes_1.insertedId,
})
let Awards_2 = db.Awards.insertOne({
    place_in_competition: 1,
    competition: Competitions_2.insertedId,
    athlet: Athletes_2.insertedId,
})
let Awards_3 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_3.insertedId,
    athlet: Athletes_3.insertedId,
})
let Awards_4 = db.Awards.insertOne({
    place_in_competition: 1,
    competition: Competitions_4.insertedId,
    athlet: Athletes_4.insertedId,
})
let Awards_5 = db.Awards.insertOne({
    place_in_competition: 2,
    competition: Competitions_5.insertedId,
    athlet: Athletes_5.insertedId,
})
let Awards_6 = db.Awards.insertOne({
    place_in_competition: 2,
    competition: Competitions_2.insertedId,
    athlet: Athletes_2.insertedId,
})
let Awards_7 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_4.insertedId,
    athlet: Athletes_1.insertedId,
})
let Awards_8 = db.Awards.insertOne({
    place_in_competition: 1,
    competition: Competitions_3.insertedId,
    athlet: Athletes_3.insertedId,
})
let Awards_9 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_3.insertedId,
    athlet: Athletes_1.insertedId,
})
let Awards_10 = db.Awards.insertOne({
    place_in_competition: 2,
    competition: Competitions_1.insertedId,
    athlet: Athletes_5.insertedId,
})
let Awards_11 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_4.insertedId,
    athlet: Athletes_2.insertedId,
})
let Awards_12 = db.Awards.insertOne({
    place_in_competition: 3,
    competition: Competitions_3.insertedId,
    athlet: Athletes_4.insertedId,
})
let Awards_13 = db.Awards.insertOne({
    place_in_competition: 1,
    competition: Competitions_3.insertedId,
    athlet: Athletes_1.insertedId,
})