const express = require("express");
const app = express();
const urlencodedParser = express.urlencoded({extended: false});

const MongoClient = require("mongodb").MongoClient;
app.use(express.static(__dirname + '/public'));
app.get("/", function (request, response) {
    response.sendFile(__dirname + "/index.html");
});

app.post("/choise", urlencodedParser, function (request, response) {
    async function connectToDatabase() {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
        const db = client.db("Semestr");

        let b = request.body.collect;
        if (b === "Types_of_sports"){
            const collection = db.collection(`${b}`);

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertTypes_of_sports" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 100px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        a = a + "<br>" + "№: " + results[i]._id + "<br>" + "title: " + results[i].title
                            + "<div style='display: flex; width: 600px'>" + "<br>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>title</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";


                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Types_of_sports");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                .toArray(function(err, results){
                                    let c = "<form action=\"/insertTypes_of_sports" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                        + "<label>\n" +
                                        "        <textarea name=\"val1\" style='width: 600px; height: 100px'></textarea>" +
                                        "    </label>"
                                        + "</form>";
                                    for(let i = 0; i < results.length; i++){
                                        c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title
                                            + "<div style='display: flex; width: 600px'>"
                                            + "<br>"
                                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                            "        <select size=\"1\" name=\"opt\">\n" +
                                            "            <option>title</option>\n" +
                                            "        </select>\n" +
                                            "    </label>"
                                            + "<label>\n" +
                                            "        <input name=\"val\" type=\"text\">\n" +
                                            "    </label>"
                                            + "</form>"
                                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                            + "</br>" + "</br>" + "</div>";
                                    }
                                    response.send(c);
                                    client.close();
                                });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Types_of_sports");

                                let t = request.body.val;
                                let y = request.body.opt;                                

                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertTypes_of_sports" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 100px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<br>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertTypes_of_sports", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Types_of_sports");

                                let t = request.body.val1;                               

                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertTypes_of_sports" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 100px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<br>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        else if (b === "Coaches"){
            const collection = db.collection(`${b}`);            

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertCoaches" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 220px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        a = a + "<br>" + "№: " + results[i]._id + "</br>" + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "work_experience: " + results[i].work_experience + "</br>"  + "sport_type_id: " + results[i].sport_type
                            + "<div style='display: flex; width: 600px'>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>first_name</option>\n" +
                            "            <option>last_name</option>\n" +
                            "            <option>patronymic</option>\n" +
                            "            <option>work_experience</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";

                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Coaches");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCoaches" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 220px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "work_experience: " + results[i].work_experience + "</br>"  + "sport_type_id: " + results[i].sport_type
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>work_experience</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Coaches");

                                let t = request.body.val;
                                let y = request.body.opt;                                

                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCoaches" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 220px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>" + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "work_experience: " + results[i].work_experience + "</br>"  + "sport_type_id: " + results[i].sport_type
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>work_experience</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertCoaches", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Coaches");

                                let t = request.body.val1;                                

                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCoaches" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 220px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "work_experience: " + results[i].work_experience + "</br>"  + "sport_type_id: " + results[i].sport_type
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>work_experience</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        else if (b === "Awards"){
            const collection = db.collection(`${b}`);            

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertAwards" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        a = a + "<br>" + "№: " + results[i]._id + "</br>"  + "place_in_competition: " + results[i].place_in_competition + "</br>"  + "competition_id: " + results[i].competition + "</br>"  + "athlet: " + results[i].athlet
                            + "<div style='display: flex; width: 600px'>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>place_in_competition</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";

                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Awards");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAwards" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "place_in_competition: " + results[i].place_in_competition + "</br>"  + "competition_id: " + results[i].competition + "</br>"  + "athlet: " + results[i].athlet
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>place_in_competition</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Awards");

                                let t = request.body.val;
                                let y = request.body.opt;                                

                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAwards" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "place_in_competition: " + results[i].place_in_competition + "</br>"  + "competition_id: " + results[i].competition + "</br>"  + "athlet: " + results[i].athlet
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>place_in_competition</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertAwards", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Awards");

                                let t = request.body.val1;                                

                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAwards" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "place_in_competition: " + results[i].place_in_competition + "</br>"  + "competition_id: " + results[i].competition + "</br>"  + "athlet: " + results[i].athlet
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>place_in_competition</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        else if (b === "Sports_facilities"){
            const collection = db.collection(`${b}`);            

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertSports_facilities" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        let l = "[";
                        for(let j = 0; j < results[i].attributes.length; j++) {
                            l = l + "</br>" + "{title: " + results[i].attributes[j].title + "</br>"  + "meaning: " + results[i].attributes[j].meaning + "</br>"  + "measure_unit: " + results[i].attributes[j].measure_unit + "}" + "</br>";
                        }
                        l = l + "]";
                        a = a + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "address: " + results[i].address + "</br>"  + "category: " + results[i].category + "</br>"  + "cattributes: " + l
                            + "<div style='display: flex; width: 600px'>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>title</option>\n" +
                            "            <option>address</option>\n" +
                            "            <option>category</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";

                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Sports_facilities");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertSports_facilities" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            let l = "[";
                                            for(let j = 0; j < results[i].attributes.length; j++) {
                                                l = l + "</br>" + "{title: " + results[i].attributes[j].title + "</br>"  + "meaning: " + results[i].attributes[j].meaning + "</br>"  + "measure_unit: " + results[i].attributes[j].measure_unit + "}" + "</br>";
                                            }
                                            l = l + "]";
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "address: " + results[i].address + "</br>"  + "category: " + results[i].category + "</br>"  + "cattributes: " + l
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>address</option>\n" +
                                                "            <option>category</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Sports_facilities");

                                let t = request.body.val;
                                let y = request.body.opt;                                

                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertSports_facilities" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            let l = "[";
                                            for(let j = 0; j < results[i].attributes.length; j++) {
                                                l = l + "</br>" + "{title: " + results[i].attributes[j].title + "</br>"  + "meaning: " + results[i].attributes[j].meaning + "</br>"  + "measure_unit: " + results[i].attributes[j].measure_unit + "}" + "</br>";
                                            }
                                            l = l + "]";
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "address: " + results[i].address + "</br>"  + "category: " + results[i].category + "</br>"  + "cattributes: " + l
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>address</option>\n" +
                                                "            <option>category</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertSports_facilities", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Sports_facilities");

                                let t = request.body.val1
                            
                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertSports_facilities" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            let l = "[";
                                            for(let j = 0; j < results[i].attributes.length; j++) {
                                                l = l + "</br>" + "{title: " + results[i].attributes[j].title + "</br>"  + "meaning: " + results[i].attributes[j].meaning + "</br>"  + "measure_unit: " + results[i].attributes[j].measure_unit + "}" + "</br>";
                                            }
                                            l = l + "]";
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "address: " + results[i].address + "</br>"  + "category: " + results[i].category + "</br>"  + "cattributes: " + l
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>address</option>\n" +
                                                "            <option>category</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        else if (b === "Athletes"){
            const collection = db.collection(`${b}`);            

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertAthletes" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        a = a + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "date_of_birth: " + results[i].date_of_birth + "</br>"  + "types_of_sports: " + results[i].types_of_sports + "</br>"  + "sports_club: " + results[i].sports_club
                            + "<div style='display: flex; width: 600px'>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>first_name</option>\n" +
                            "            <option>last_name</option>\n" +
                            "            <option>patronymic</option>\n" +
                            "            <option>date_of_birth</option>\n" +
                            "            <option>sports_club</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";

                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Athletes");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAthletes" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "date_of_birth: " + results[i].date_of_birth + "</br>"  + "types_of_sports: " + results[i].types_of_sports + "</br>"  + "sports_club: " + results[i].sports_club
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>date_of_birth</option>\n" +
                                                "            <option>sports_club</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Athletes");

                                let t = request.body.val;
                                let y = request.body.opt;                                

                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAthletes" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "date_of_birth: " + results[i].date_of_birth + "</br>"  + "types_of_sports: " + results[i].types_of_sports + "</br>"  + "sports_club: " + results[i].sports_club
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>date_of_birth</option>\n" +
                                                "            <option>sports_club</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertAthletes", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Athletes");

                                let t = request.body.val1;
                                
                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertAthletes" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "first_name: " + results[i].first_name + "</br>"  + "last_name: " + results[i].last_name + "</br>"  + "patronymic: " + results[i].patronymic + "</br>"  + "date_of_birth: " + results[i].date_of_birth + "</br>"  + "types_of_sports: " + results[i].types_of_sports + "</br>"  + "sports_club: " + results[i].sports_club
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>first_name</option>\n" +
                                                "            <option>last_name</option>\n" +
                                                "            <option>patronymic</option>\n" +
                                                "            <option>date_of_birth</option>\n" +
                                                "            <option>sports_club</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        else if (b === "Competitions"){
            const collection = db.collection(`${b}`);            

            collection.find()
                .toArray(function(err, results){
                    let a = "<form action=\"/insertCompetitions" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                        + "<label>\n" +
                        "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                        "    </label>"
                        + "</form>";
                    for(let i = 0; i < results.length; i++){
                        a = a + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "date_of_the_event: " + results[i].date_of_the_event + "</br>"  + "sports_facility_id: " + results[i].sports_facility + "</br>"  + "sport_type_id: " + results[i].sport_type + "</br>"  + "sports_organization: " + results[i].sports_organization + "</br>"  + "athletes: " + results[i].athletes
                            + "<div style='display: flex; width: 600px'>"
                            + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                            "        <select size=\"1\" name=\"opt\">\n" +
                            "            <option>title</option>\n" +
                            "            <option>date_of_the_event</option>\n" +
                            "            <option>sports_organization</option>\n" +
                            "        </select>\n" +
                            "    </label>"
                            + "<label>\n" +
                            "        <input name=\"val\" type=\"text\">\n" +
                            "    </label>"
                            + "</form>"
                            + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                            + "</br>" + "</br>" + "</div>";

                        app.post("/delete" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Competitions");

                                collection.deleteOne({_id: results[i]._id});

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCompetitions" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "date_of_the_event: " + results[i].date_of_the_event + "</br>"  + "sports_facility_id: " + results[i].sports_facility + "</br>"  + "sport_type_id: " + results[i].sport_type + "</br>"  + "sports_organization: " + results[i].sports_organization + "</br>"  + "athletes: " + results[i].athletes
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>date_of_the_event</option>\n" +
                                                "            <option>sports_organization</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/update" + results[i]._id, urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Competitions");

                                let t = request.body.val;
                                let y = request.body.opt;
                                
                                collection.updateOne({_id: results[i]._id}, {$set: {[y] : t }})

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCompetitions" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "date_of_the_event: " + results[i].date_of_the_event + "</br>"  + "sports_facility_id: " + results[i].sports_facility + "</br>"  + "sport_type_id: " + results[i].sport_type + "</br>"  + "sports_organization: " + results[i].sports_organization + "</br>"  + "athletes: " + results[i].athletes
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>date_of_the_event</option>\n" +
                                                "            <option>sports_organization</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                        app.post("/insertCompetitions", urlencodedParser, function (request, response) {
                            async function connectToDatabase() {
                                const client = await MongoClient.connect('mongodb://127.0.0.1:27017')
                                const db = client.db("Semestr");
                                const collection = db.collection("Competitions");

                                let t = request.body.val1;

                                collection.insertOne(JSON.parse(t));

                                collection.find()
                                    .toArray(function(err, results){
                                        let c = "<form action=\"/insertCompetitions" +"\" method=\"post\"> " + "<p>" + "  <input type=\"submit\" value=\"Добавить\" />" + "</p>"
                                            + "<label>\n" +
                                            "        <textarea name=\"val1\" style='width: 600px; height: 300px'></textarea>" +
                                            "    </label>"
                                            + "</form>";
                                        for(let i = 0; i < results.length; i++){
                                            c = c + "<br>" + "№: " + results[i]._id + "</br>"  + "title: " + results[i].title + "</br>"  + "date_of_the_event: " + results[i].date_of_the_event + "</br>"  + "sports_facility_id: " + results[i].sports_facility + "</br>"  + "sport_type_id: " + results[i].sport_type + "</br>"  + "sports_organization: " + results[i].sports_organization + "</br>"  + "athletes: " + results[i].athletes
                                                + "<div style='display: flex; width: 600px'>"
                                                + "<form action=\"/update" + results[i]._id +"\" method=\"post\" style='margin: auto; width: 100px'> " + "<p>" + "  <input type=\"submit\" value=\"Изменить\" />" + "</p>" + "<label>\n" +
                                                "        <select size=\"1\" name=\"opt\">\n" +
                                                "            <option>title</option>\n" +
                                                "            <option>date_of_the_event</option>\n" +
                                                "            <option>sports_organization</option>\n" +
                                                "        </select>\n" +
                                                "    </label>"
                                                + "<label>\n" +
                                                "        <input name=\"val\" type=\"text\">\n" +
                                                "    </label>"
                                                + "</form>"
                                                + "<form action=\"/delete" + results[i]._id +"\" method=\"post\" style='margin: auto'> " + "<p>" + "  <input type=\"submit\" value=\"Удалить\"/>" + "</p>" + "</form>"
                                                + "</br>" + "</br>" + "</div>";
                                        }
                                        response.send(c);
                                        client.close();
                                    });
                            };
                            connectToDatabase();
                        });
                    }
                    response.send(a);
                    client.close();
                });
        }
        console.log(b);
    };
    connectToDatabase();
});
app.listen(3000);