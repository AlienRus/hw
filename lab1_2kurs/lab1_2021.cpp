﻿#include <iostream>
#include <time.h>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

struct datetime
{
    int seconds;
    int minute;
    int hour;

    int day;
    int month;
    int year;

    void Output_info();
    void System_time(datetime& system);
    void Get_next_date(datetime& next);
    void Get_prev_date(datetime& prev);
};

void datetime::Output_info()
{
    printf("%i:%i:%i %i/%i/%i", hour, minute, seconds, day, month, year);
}

void datetime::System_time(datetime& system)
{
    time_t rawtime;
    tm timeinfo;
    time(&rawtime);
    localtime_s(&timeinfo, &rawtime);

    system.hour = timeinfo.tm_hour;
    system.minute = timeinfo.tm_min;
    system.seconds = timeinfo.tm_sec;
    system.day = timeinfo.tm_mday;
    system.month = timeinfo.tm_mon + 1;
    system.year = timeinfo.tm_year + 1900;
}

time_t Get_unix_time(datetime A)
{
    tm timeinfo;
    timeinfo.tm_year = A.year - 1900;
    timeinfo.tm_mon = A.month - 1;
    timeinfo.tm_mday = A.day;
    timeinfo.tm_hour = A.hour;
    timeinfo.tm_min = A.minute;
    timeinfo.tm_sec = A.seconds;
    time_t unix_time = mktime(&timeinfo);
    return unix_time;
}

void Get_normal_time(time_t unix_time, datetime& A)
{
    tm timeinfo;
    localtime_s(&timeinfo, &unix_time);
    A.hour = timeinfo.tm_hour;
    A.minute = timeinfo.tm_min;
    A.seconds = timeinfo.tm_sec;
    A.day = timeinfo.tm_mday;
    A.month = timeinfo.tm_mon + 1;
    A.year = timeinfo.tm_year + 1900;
}

void datetime::Get_next_date(datetime& next)
{
    int min = 2419200 * 2;
    int max = 31536000;
    Get_normal_time(Get_unix_time(*this) + (min + rand() % (max - min + 1)), next);
}

void datetime::Get_prev_date(datetime& prev)
{
    int min = 2419200 * 2;
    int max = 31536000;
    Get_normal_time(Get_unix_time(*this) - (min + rand() % (max - min + 1)), prev);
}

datetime* Read_file_date(string file_name, int& n)
{
    ifstream file(file_name);
    string line;
    n = 0;
    while (getline(file, line))
    {
      n++;
    }
    datetime* Date = new datetime[n];

    int i = 0;
    file.clear();
    file.seekg(0, ios::beg);
    while (getline(file, line))
    {
        istringstream line_(line);
        line_ >> Date[i].hour >> Date[i].minute >> Date[i].seconds >> Date[i].day >> Date[i].month >> Date[i].year;
        i++;
    }
    file.close();
    return Date;
}


int main()
{
    setlocale(LC_ALL, "Russian");

    srand(time(0));
    datetime date, next, prev;
    int n;
    datetime* Date = Read_file_date("data.txt", n);
    datetime* Next = new datetime[n];
    date.System_time(date); printf("\n\nСистемное время: "); 
    date.Output_info();
    printf("\n Считывание с файла: \tДата | След. дата | Пред. дата\n");

    for (int i = 0; i < n; i++)
    {
        Date[i].Get_next_date(next);
        Date[i].Get_prev_date(prev);
        printf("%i)\t", i + 1);
        Date[i].Output_info();
        printf("\t");
        next.Output_info();
        printf("\t");
        prev.Output_info();
        printf("\n");
        Next[i] = next;
    }

    printf("\nЗадание по варианту:\n Вариант 13\n");
    for (int i = 0; i < n; i++)
    {
        if ((((Date[i].day)%2) == 0) && (((Next[i].day)%2) != 0))
        {
            printf("%i)\t", i + 1);
            Date[i].Output_info();
            printf("\t");
            Next[i].Output_info();
            printf("\n");
        }
    }
    delete[] Date;
    delete[] Next;
    return 0;
}