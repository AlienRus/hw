﻿#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
using namespace std;
#define RAND_MAX (rand() % 1000)

double* CreateArr(int);
void FillArr(double*, int);
void PrintArr(double*, int);
void T1(double*&, int&, int);
void del(double*&, int&, int);
int k();
void T3(double*&, int&, double);
void T4(double*&, int&, double);

int main() {
    setlocale(LC_ALL, "Russian");
    int n = k();
    if (n <= 0) cout << "Файл отсутствует или содержит недопустимое содержание\n";

    double* arr = CreateArr(n);
    FillArr(arr, n);
    if (n!=0) cout << "Считывание с файла прошло успешно, получившийся массив: "; PrintArr(arr, n);
    int choice_menu;
    do {
        cout << "\n\nЗадание 1: Добавить элемент на любую позицию\nЗадание 2: Удалить элемент с любой позиции\nЗадание 3: Добавить элемент массива после первого встреченного элемента с заданным значением \nЗадание 4: Удалить все элементы, равные введенному значению\n0: Выход\nВыберите задание: ";
        cin >> choice_menu;
        switch (choice_menu) {
        case 1:
            double r; int k;
            cout << "\nИсходный массив:\n"; PrintArr(arr, n); cout << "\nВведите место для внесения: ";
            cin >> k;            
            while (k > n+1 || k<1)
            {
                cout << "Такого места в данном массиве нет, повторите попытку:"; cin >> k;
            } 
            T1(arr, n, k);    
            PrintArr(arr, n);
            break;
        case 2:
            int f; 
            cout << "\nИсходный массив:\n";
            PrintArr(arr, n);
            cout << "\nВведите позицию элемента для удаления: "; cin >> f;
            while (f > n + 1 || f < 1)
            {
                cout << "Такого места в данном массиве нет, повторите попытку:"; cin >> f;
            }
            del(arr, n, f);
            PrintArr(arr, n);
            break;
        case 3:
            double z;
            cout << "\nИсходный массив:\n";
            PrintArr(arr, n);
            cout << "Введите значение элемента, после которого добавить число: "; cin >> z;
            T3(arr, n, z);
            PrintArr(arr, n);
            break;
        case 4:
            double z1;
            cout << "\nИсходный массив:\n";
            PrintArr(arr, n);
            cout << "Введите значение элементов, которые необходимо удалить: "; cin >> z1;
            T4(arr, n, z1);
            PrintArr(arr, n);
            break;
        case 0:
            cout << "Goodbye\n";
            break;
            if (choice_menu != 0) {
                cout << endl << "Нажмите Enter чтобы продолжить\n";
            }
        }
    } while (choice_menu);



    delete[] arr;
}

int k() {
    double a; int k = 0;
    fstream A("array_data.txt", ios::in);
    while (A >> a) k++;
    A.close();
    return k;
}

double* CreateArr(int n) {
    double* arr = new double[n];
    return arr;
}

void PrintArr(double* arr, int n) {
    for (int i = 0; i < n; i++) {
        cout << arr[i] << ", ";
    }
    cout << endl;
}

void FillArr(double* arr, int n) {
    fstream A1("array_data.txt", ios::in);
    for (int i = 0; i < n; i++)
    {
        A1 >> arr[i];
    }
    A1.close();
}

void T1(double*& arr, int& n, int k)
{
    int q;
    double r=0;  
        double* buff = new double[n + 1];
        for (int i = 0; i < k - 1; i++) {
            buff[i] = arr[i];
        }

        do {
            cout << "Ввести число для вставки с клавиатуры(1) или вставить случаное(2)?\n";
            cin >> q;
            if (q == 1) {
                cout << "Введите число для вставки: "; cin >> r;
            }
            else
            {
                if (q == 2)
                {
                    srand(time(NULL));
                    r = (double)(rand()) / RAND_MAX;
                }
                else cout << "Введено некорректное число\n";                 
            }
        } while (q == 0 || q > 2);

        buff[k - 1] = r;
        for (int i = k; i < n + 1; i++) {
            buff[i] = arr[i - 1];
        }
        delete[] arr;
        arr = buff;
        n++;   
}

void del(double*& arr, int& n, int k)
{
    double r;    
        double* buff = new double[n - 1];
        for (int i = 0; i < k - 1; i++) {
            buff[i] = arr[i];
        }
        n--;
        for (int i = k - 1; i < n + 1; i++) {
            buff[i] = arr[i + 1];
        }
        delete[] arr;
        arr = buff; 
}

void T3(double*& arr, int& n, double z)
{
    int q, k = 0, bn=n;
    double r = 0;
    double* buff = new double[n + 1];
    for (int i = 0; i < n; i++)
    {
        if (arr[i] == z)
        {
            k = i;
            double* buff = new double[n + 1];
            for (int i = 0; i <= k; i++) {
                buff[i] = arr[i];
            }

            do {
                cout << "Ввести число для вставки с клавиатуры(1) или вставить случаное(2)?\n";
                cin >> q;
                if (q == 1) {
                    cout << "Введите число для вставки: "; cin >> r;
                }
                else
                {
                    if (q == 2)
                    {
                        srand(time(NULL));
                        r = (double)(rand()) / RAND_MAX;
                    }
                    else cout << "Неверно введено число, повторите попытку\n";
                }
            } while (q == 0 || q > 2);

            buff[k+1] = r;
            for (int i = k+2; i < n + 1; i++) {
                buff[i] = arr[i - 1];
            }
            delete[] arr;
            arr = buff;
            n++; i++;
            break;
        }
    }
    if (bn == n) cout << "Элемент с таким значением не найден в массиве\n"; 
}

void T4(double*& arr, int& n, double z)
{
    int bn=n, k;
    for (int i = 0; i < n; i++)
    {
        if (arr[i] == z)
        {
            k = i + 1;
            del(arr, n, k);
            i--;
        }
    }
    if (bn == n) cout << "Элемент с таким значением не найден в массиве\n";
}