#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>
#include "Application.h"
#include "UI.h"

using namespace std;

void Application::Run(string fileName)
{
    srand(time(0));
    Datetime date, next, prev;
    DatetimeRandom randomDate;
    UI ui;
    int n;
    bool info = false;
    Datetime* Date = GetDate(fileName, n);
    Datetime* Next = new Datetime[n];
    date.SystemTime(date);
    ui.PrintSystemDate(date);
    ui.Info(info);
    for (int i = 0; i < n; i++)
    {
        randomDate.date = Date[i];
        next = randomDate.NextDate();
        prev = randomDate.PrevDate();
        Next[i] = next;
        ui.PrintDateNextPrev(Date[i], next, prev, i);
    }
    info = true;
    ui.Info(info);
    for (int i = 0; i < n; i++)
    {
        if ((((Date[i].Get(_day_) % 2) == 0) && (((Next[i].Get(_day_) % 2) != 0))))
        {
            ui.PrintDateNextPrev(Date[i], Next[i], i);
        }
    }
    delete[] Date;
    delete[] Next;
}

Datetime* Application::GetDate(string fileName, int& n)
{
    int hour, minute, second, day, month, year;
    ifstream file(fileName);
    string line;
    n = 0;
    while (getline(file, line))
    {
        n++;
    }
    Datetime* Date = new Datetime[n];
    int i = 0;
    file.clear();
    file.seekg(0, ios::beg);
    while (getline(file, line))
    {
        istringstream line_(line);
        line_ >> hour >> minute >> second >> day >> month >> year;
        Date[i] = Datetime(hour, minute, second, day, month, year);
        i++;
    }
    file.close();
    return Date;
}